#include<iostream>
using namespace std;
#include "Time.h"

int main()
{
	// create a time object
    Time t1(10,50,59);
    Time t3(12,13,72); 
    //added a new time set t3
    t1.print();
	// create a default time object
    Time t2;
    t2.print();
	// set time object and print
    t2.setTime(6,39,9);
    t2.print();

	// compare t1 and t2


    if(t1.equals(t2))
    {
        cout<<"Two objects are equal\n";
    }
    else
    {
        cout << "Two objects are not equal\n";
    }

    return 0;
}
