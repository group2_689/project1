#include<iostream>
#include<iomanip>
#include "Time.h"
using namespace std;

//sk2998 edited
Time::Time(const int h,const int m,const int s)
   : hour(h),minute(m),second(s)
{}

void Time :: setTime(const int h,const int m,const int s)
{
    hour =h;
    minute =m;
    second =s;
}
//the function is used to print time 
void Time::print() const
{
    cout<<setw(2)<<setfill('0')<<hour<<":"<<setw(2)<<setfill('0')<<minute<<":"<<setw(2)<<setfill('0')<<second<<"\n";

}

bool Time:: equals(const Time &otherTime)
{
    if (hour==otherTime.hour && minute==otherTime.minute && second==otherTime.second){
        return true;
    }
    else
    {
        return false;
    }
}


