// Header file for Time class
#ifndef TIME_H
#define TIME_H

class Time
{   
    // class attributes

    private : 
         int hour;
         int minute;
         int second;

    public :
    // with default value
        Time(const int h=0,const int m=0,const int s=0);
    // class methods

    //setter time
        void setTime(const int h,const int m,const int s);
    //print
        void print() const;
    //compare 2 time object
        bool equals(const Time&);

};

#endif
